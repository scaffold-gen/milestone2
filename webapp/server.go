package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/arjanvaneersel/scaffold-gen/webapp/handlers"
)

func NewServer(addr string) http.Server {
	srv := http.Server{
		Addr:    addr,
		Handler: handlers.Setup(),
	}

	return srv
}

func main() {
	srv := NewServer(":8000")
	if err := srv.ListenAndServe(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
