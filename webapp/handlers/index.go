package handlers

import "net/http"

const html = `
<!DOCTYPE html>
<html>
  <head>
    <title>Homepage - Project Name</title>
    <link rel="stylesheet" href="static/css/styles.css" />
    <script async src="static/js/index.js"></script>
  </head>
  <body>
    <h1>Echorand Corp. This is the homepage for project Project Name.</h1>
  </body>
</html>`

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(html))
}
